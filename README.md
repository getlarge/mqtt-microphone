# MQTT Microphone Streamer 

**microphone-mqtt** is a **Arduino - ESP32-core - PubSubClient** embedded application, which works in conjunction with [Snips](https://github.com/snipsco).

## Table of Contents
- [Features](#features)
- [Setup](#setup)
- [Installation](#installation)
- [Usage](#usage)

## Features

+ real-time sound recording with MAX4466 mic
+ connect to mqtt broker
+ easily choose audio buffer formatting
+ send nicely sliced sound buffer via MQTT ( details soon )

## Setup

+ [Arduino IDE](https://www.arduino.cc/en/main/software)
+ [ESP32-core](https://github.com/espressif/arduino-esp32)
+ [PubSubClient](https://github.com/knolleary/pubsubclient)

Change MQTT  buffer size and keepalive ( PubSubClient.h )

## Installation



## Usage



## TODO

+ add Wav headers
+ handle Snips MQTT protocol

