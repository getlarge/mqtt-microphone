/* Audio streamer with ESP32 and Adafruit elected microphone board. 
 * Created by Julian Schroeter.
*/
#include <Arduino.h>
#include <WiFi.h>
#include <driver/adc.h>

#include <PubSubClient.h>

#define AUDIO_BUFFER_MAX 800
#define MY_DEBUG

uint8_t audioBuffer[AUDIO_BUFFER_MAX];
uint8_t transmitBuffer[AUDIO_BUFFER_MAX];
uint32_t bufferPointer = 0;
bool transmitNow = false;

const char* ssid     = "TNCAP16AEDD";
const char* password = "7EB398D2C2";
const char* host     = "YOUR SERVER IP ADDRESS";
char broker[] = "server2.getlarge.eu";
const int port = 1884;
const char* clientUser = "user";
const char* clientPass = "motdepasse";

long lastMsg = 0;
char msg[50];
int value = 0;
long lastReconnectAttempt = 0;

WiFiClient wifiClient;
//PubSubClient client(broker, port, callback, wifiClient);
PubSubClient client(wifiClient);
int status = WL_IDLE_STATUS;

hw_timer_t * timer = NULL; // our timer
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED; 



////Snips formatting
//uint16_t buffer[256];
//while (true) {
//  if (connected) {
//    mics.Read();
//    
//    //NumberOfSamples() = kMicarrayBufferSize / kMicrophoneChannels = 2048 / 8 = 256
//    for (uint32_t s = 0; s < mics.NumberOfSamples(); s++) {
//        buffer[s] = mics.Beam(s);
//    }    
//    esp_mqtt_publish("hermes/audioServer/voice/audioFrame", (uint8_t *)buffer, sizeof(buffer),0, false);
//  }
//}


    
void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux); // says that we want to run critical code and don't want to be interrupted
  int adcVal = adc1_get_voltage(ADC1_CHANNEL_0); // reads the ADC
  uint8_t value = map(adcVal, 0 , 4096, 0, 255);  // converts the value to 0..255 (8bit)
  
  audioBuffer[bufferPointer] = value; // stores the value
  bufferPointer++;
 
  if (bufferPointer == AUDIO_BUFFER_MAX) { // when the buffer is full
    bufferPointer = 0;
    memcpy(transmitBuffer, audioBuffer, AUDIO_BUFFER_MAX); // copy buffer into a second buffer
    transmitNow = true; // sets the value true so we know that we can transmit now
  }
  portEXIT_CRITICAL_ISR(&timerMux); // says that we have run our critical code
}

//void callback(char* topic, byte* payload, unsigned int length) {
//  // In order to republish this payload, a copy must be made
//  // as the orignal payload buffer will be overwritten whilst
//  // constructing the PUBLISH packet.
//
//  // Allocate the correct amount of memory for the payload copy
//  byte* p = (byte*)malloc(length);
//  // Copy the payload to the new buffer
//  memcpy(p,payload,length);
//  client.publish("outTopic", p, length);
//  // Free the memory
//  free(p);
//}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void setup_wifi() {
  //delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

boolean reconnect_mqtt() {
    String clientId = "ESP32-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str(), clientUser, clientPass)) {
        client.publish("esp32-out","hello world");
        client.subscribe("esp32-in");
        Serial.println(F("MQTT connected"));
    }
    return client.connected();
}

void setup() {
    Serial.begin(115200);
    setup_wifi();
    adc1_config_width(ADC_WIDTH_12Bit); // configure the analogue to digital converter
    adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_0db); // connects the ADC 1 with channel 0 (GPIO 36)
    client.setServer( broker, port );
    client.setCallback(callback);
    lastReconnectAttempt = 0;
    timer = timerBegin(0, 80, true); // 80 Prescaler
    timerAttachInterrupt(timer, &onTimer, true); // binds the handling function to our timer 
    timerAlarmWrite(timer, 125, true);
    timerAlarmEnable(timer);
}

void loop() {
    if (!client.connected()) {
        long now = millis();
    if (now - lastReconnectAttempt > 5000) {
        lastReconnectAttempt = now;
        if (reconnect_mqtt()) {
            Serial.print(F("Attempting MQTT connection to :"));
            Serial.println(broker);
            lastReconnectAttempt = 0;
        }
    }
    } else {
       if (transmitNow) { // checks if the buffer is full
          transmitNow = false;
    //      ++value;
    //      snprintf (msg, 75, "hello world #%ld", value);
          Serial.print(F("Publish message: "));
          //Serial.println(msg);
          //client.publish("outTopic", msg);
          //Serial.write((const uint8_t *)audioBuffer, sizeof(audioBuffer));
          client.publish("outTopic", (const uint8_t *)audioBuffer, sizeof(audioBuffer));
        }
        client.loop();
    }

}

